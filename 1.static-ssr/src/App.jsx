import 'isomorphic-fetch';
import 'babel-polyfill';
import React from 'react';
import { hot } from 'react-hot-loader';
import HomePage from './pages/Home';

const App = () => (
  <div>
    <h1>Server Side Renderig</h1>
    <HomePage />
  </div>
);

export default hot(module)(App);

