import React from 'react';
import styles from './Home.css';

const Home = () => (
  <div>
    <h2 className={styles.title}>Home Page</h2>
  </div>
);

export default Home;
