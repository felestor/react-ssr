import 'isomorphic-fetch';
import 'babel-polyfill';
import React from 'react';
import { hot } from 'react-hot-loader';
import Home from './pages/Home';

const App = () => (
  <div>
    <h1>Server Side Renderig</h1>
    <Home />
  </div>
);

export default hot(module)(App);

